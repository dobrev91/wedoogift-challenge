package com.wedoogift.backendchallenge.service;

import com.wedoogift.backendchallenge.exception.InsufficientBalanceException;
import com.wedoogift.backendchallenge.utils.DateUtils;
import com.wedoogift.backendchallenge.model.Company;
import com.wedoogift.backendchallenge.model.Deposit;
import com.wedoogift.backendchallenge.model.User;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

@Service
public class DepositServiceImpl implements DepositService {

    private final DateUtils dateUtils;

    public DepositServiceImpl(DateUtils dateUtils) {

        this.dateUtils = dateUtils;
    }

    public User depositGift(User user, Company company, BigDecimal amount) throws InsufficientBalanceException {

        if (company.getBalance().compareTo(amount) >= 0) {
            user.addDeposit(new Deposit(amount, dateUtils.currentDate().plusDays(364)));
        } else {
            throw new InsufficientBalanceException(company.getName() + " balance is insufficient");
        }
        return user;
    }

    public User depositMeal(User user, Company company, BigDecimal amount) throws InsufficientBalanceException {

        if (company.getBalance().compareTo(amount) >= 0) {
            LocalDate currentDate = dateUtils.currentDate();
            LocalDate expirationDate = LocalDate.of(currentDate.getYear() + 1, 2, 1);

            user.addDeposit(new Deposit(amount, expirationDate.with(TemporalAdjusters.lastDayOfMonth())));
        } else {
            throw new InsufficientBalanceException(company.getName() + " balance is insufficient");
        }
        return user;
    }

    public BigDecimal getUserBalance(User user) {

        return user.getDeposits().stream()
            .filter(deposit -> !LocalDate.now().isAfter(deposit.getExpirationDate()))
            .map(Deposit::getAmount)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
