package com.wedoogift.backendchallenge.service;

import com.wedoogift.backendchallenge.exception.InsufficientBalanceException;
import com.wedoogift.backendchallenge.model.Company;
import com.wedoogift.backendchallenge.model.User;

import java.math.BigDecimal;

public interface DepositService {

    User depositGift(User user, Company company, BigDecimal amount) throws InsufficientBalanceException;

    User depositMeal(User user, Company company, BigDecimal amount) throws InsufficientBalanceException;

    BigDecimal getUserBalance(User user);
}
