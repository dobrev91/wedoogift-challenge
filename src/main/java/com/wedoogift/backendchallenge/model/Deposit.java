package com.wedoogift.backendchallenge.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Deposit {

    private final BigDecimal amount;
    private final LocalDate expirationDate;

    public Deposit(BigDecimal amount, LocalDate expirationDate) {

        this.amount = amount;
        this.expirationDate = expirationDate;
    }

    public BigDecimal getAmount() {

        return amount;
    }

    public LocalDate getExpirationDate() {

        return expirationDate;
    }
}
