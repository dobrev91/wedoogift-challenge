package com.wedoogift.backendchallenge.model;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class User {

    public final String id;
    public final List<Deposit> deposits = new ArrayList<>();

    public User(String id, List<Deposit> deposits) {

        this.id = id;
        if (CollectionUtils.isNotEmpty(deposits)) {
            this.deposits.addAll(deposits);
        }
    }

    public String getId() {

        return id;
    }

    public List<Deposit> getDeposits() {

        return Collections.unmodifiableList(deposits);
    }

    public void addDeposit(Deposit deposit) {
        this.deposits.add(deposit);
    }
}
