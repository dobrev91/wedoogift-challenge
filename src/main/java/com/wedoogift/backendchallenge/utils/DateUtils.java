package com.wedoogift.backendchallenge.utils;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DateUtils {

    public LocalDate currentDate() {

        return LocalDate.now();
    }

}
