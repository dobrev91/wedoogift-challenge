package com.wedoogift.backendchallenge.service;

import com.wedoogift.backendchallenge.exception.InsufficientBalanceException;
import com.wedoogift.backendchallenge.utils.DateUtils;
import com.wedoogift.backendchallenge.model.Company;
import com.wedoogift.backendchallenge.model.Deposit;
import com.wedoogift.backendchallenge.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DepositServiceImplTest {

    private DepositService depositService;

    @MockBean
    DateUtils dateUtils;

    @BeforeEach
    void setUp() {

        dateUtils = mock(DateUtils.class);
        depositService = new DepositServiceImpl(dateUtils);
    }

    @Test
    void shouldDepositGift() throws InsufficientBalanceException {

        //Given
        User user = new User("idUser", Collections.emptyList());
        Company company = new Company("idCompany", "Wedoogift", BigDecimal.valueOf(10000));
        LocalDate mockedDate = LocalDate.of(2021, 6, 15);

        // When
        when(dateUtils.currentDate()).thenReturn(mockedDate);
        User expectedUser = depositService.depositGift(user, company, BigDecimal.valueOf(1000));

        // Then
        assertThat(expectedUser.getDeposits()).hasSize(1);
        assertThat(expectedUser.getDeposits().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(1000));
        assertThat(expectedUser.getDeposits().get(0).getExpirationDate()).isEqualTo(LocalDate.of(2022, 6, 14));
    }

    @Test
    void shouldThrowInsufficientBalanceWhenCompanyHasNotEnoughtForGift() throws InsufficientBalanceException {

        //Given
        User user = new User("idUser", Collections.emptyList());
        Company company = new Company("id", "Wedoogift", BigDecimal.valueOf(10));
        LocalDate mockedDate = LocalDate.of(2021, 6, 15);

        // When Then
        when(dateUtils.currentDate()).thenReturn(mockedDate);
        assertThatThrownBy(() -> depositService.depositGift(user, company, BigDecimal.valueOf(1000)))
            .isInstanceOf(InsufficientBalanceException.class)
            .hasMessage("Wedoogift balance is insufficient");
    }

    @Test
    void shouldDepositMeal() throws InsufficientBalanceException {

        //Given
        User user = new User("idUser", Collections.emptyList());
        Company company = new Company("idCompany", "Wedoogift", BigDecimal.valueOf(10000));
        LocalDate mockedDate = LocalDate.of(2020, 1, 1);

        // When
        when(dateUtils.currentDate()).thenReturn(mockedDate);
        User expectedUser = depositService.depositMeal(user, company, BigDecimal.valueOf(1000));

        // Then
        assertThat(expectedUser.getDeposits()).hasSize(1);
        assertThat(expectedUser.getDeposits().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(1000));
        assertThat(expectedUser.getDeposits().get(0).getExpirationDate()).isEqualTo(LocalDate.of(2021, 2, 28));
    }

    @Test
    void shouldDepositMealForExpirationDateOnLeapYear() throws InsufficientBalanceException {

        //Given
        User user = new User("idUser", Collections.emptyList());
        Company company = new Company("idCompany", "Wedoogift", BigDecimal.valueOf(10000));
        LocalDate mockedDate = LocalDate.of(2023, 1, 1);

        // When
        when(dateUtils.currentDate()).thenReturn(mockedDate);
        User expectedUser = depositService.depositMeal(user, company, BigDecimal.valueOf(1000));

        // Then
        assertThat(expectedUser.getDeposits()).hasSize(1);
        assertThat(expectedUser.getDeposits().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(1000));
        assertThat(expectedUser.getDeposits().get(0).getExpirationDate()).isEqualTo(LocalDate.of(2024, 2, 29));
    }

    @Test
    void shouldThrowInsufficientBalanceWhenCompanyHasNotEnoughtForMeal() throws InsufficientBalanceException {

        //Given
        User user = new User("idUser", Collections.emptyList());
        Company company = new Company("id", "Wedoogift", BigDecimal.valueOf(10));
        LocalDate mockedDate = LocalDate.of(2021, 6, 15);

        // When Then
        when(dateUtils.currentDate()).thenReturn(mockedDate);
        assertThatThrownBy(() -> depositService.depositMeal(user, company, BigDecimal.valueOf(1000)))
            .isInstanceOf(InsufficientBalanceException.class)
            .hasMessage("Wedoogift balance is insufficient");
    }

    @Test
    void shouldGetUserBalance() {

        // Given
        List<Deposit> deposits = Arrays.asList(
            new Deposit(BigDecimal.valueOf(500), LocalDate.of(2021, 1, 18)),
            new Deposit(BigDecimal.valueOf(1000), LocalDate.of(2022, 6, 6)),
            new Deposit(BigDecimal.valueOf(5000), LocalDate.of(2023, 3, 6)));
        User user = new User("idUser", deposits);
        LocalDate mockedDate = LocalDate.of(2022, 1, 15);

        // When
        when(dateUtils.currentDate()).thenReturn(mockedDate);
        BigDecimal balance = depositService.getUserBalance(user);
        // Then
        assertThat(balance).isEqualTo(BigDecimal.valueOf(6000));
    }
}